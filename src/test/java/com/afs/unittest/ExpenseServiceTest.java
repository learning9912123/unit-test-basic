package com.afs.unittest;

import com.afs.unittest.Project.Project;
import com.afs.unittest.Project.ProjectType;
import com.afs.unittest.exception.UnexpectedProjectTypeException;
import com.afs.unittest.expense.ExpenseType;
import org.junit.jupiter.api.Test;

import static com.afs.unittest.expense.ExpenseType.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ExpenseServiceTest {
    @Test
    void should_return_internal_expense_type_when_getExpenseCodeByProject_given_internal_project() {
        // given
        Project project = new Project(ProjectType.INTERNAL, "Pname");
        // when
        ExpenseService expenseService = new ExpenseService();
        ExpenseType expenseType = expenseService.getExpenseCodeByProject(project);
        // then
        assertEquals(INTERNAL_PROJECT_EXPENSE, expenseType);
    }

    @Test
    void should_return_expense_type_A_when_getExpenseCodeByProject_given_project_is_external_and_name_is_project_A() {
        // given
        Project project = new Project(ProjectType.EXTERNAL, "Project A");
        // when
        ExpenseService expenseService = new ExpenseService();
        ExpenseType expenseType = expenseService.getExpenseCodeByProject(project);
        // then
        assertEquals(EXPENSE_TYPE_A, expenseType);
    }

    @Test
    void should_return_expense_type_B_when_getExpenseCodeByProject_given_project_is_external_and_name_is_project_B() {
        // given
        Project project = new Project(ProjectType.EXTERNAL, "Project B");
        // when
        ExpenseService expenseService = new ExpenseService();
        ExpenseType expenseType = expenseService.getExpenseCodeByProject(project);
        // then
        assertEquals(EXPENSE_TYPE_B, expenseType);
    }

    @Test
    void should_return_other_expense_type_when_getExpenseCodeByProject_given_project_is_external_and_has_other_name() {
        // given
        Project project = new Project(ProjectType.EXTERNAL, "other");
        // when
        ExpenseService expenseService = new ExpenseService();
        ExpenseType expenseType = expenseService.getExpenseCodeByProject(project);
        // then
        assertEquals(OTHER_EXPENSE, expenseType);
    }

    @Test
    void should_throw_unexpected_project_exception_when_getExpenseCodeByProject_given_project_is_invalid() {
        // given
        Project project = new Project(ProjectType.UNEXPECTED_PROJECT_TYPE, "other");
        // when
        ExpenseService expenseService = new ExpenseService();
        // then
        assertThrows(UnexpectedProjectTypeException.class,()->expenseService.getExpenseCodeByProject(project));
    }
}